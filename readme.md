# README #

I've created a working ACL (access control list) in Laravel 5.5 

Built in Laravel's homestead. There are a few things you'll need to do to set this up.

This repo exists because of a tutorial I followed here:
https://medium.com/@ezp127/laravel-5-4-native-user-authentication-role-authorization-3dbae4049c8a

I thought it may be easier to keep it in a bitbucket repo for anyone else who would like to use it in future. 

### What is this repository for? ###

* A working ACL (access control list) in Laravel 5.5 
* Version 1.0

### How do I get set up? ###

* Summary of set up
* Hopefully you have set up Homestead before, or have laravel set up some other way. 
* Simply download these files and place them where you wanted your laravel installation.
* Modify the .env with your database details
* You should now be able to access the Laravel project with your URL. 
* 2 default users have already been created with the seeder.
* User 1 - username: manager@example.com password: secret
* User 2 - username: employee@example.com	password: secret

### Contribution guidelines ###

* :)
* Make your own branch and modify it however you like.

### Who do I talk to? ###

* Quentin Watt
* https://www.youtube.com/QuentinWatt
* https://www.twitter.com/QuentinWatt
* https://www.facebook.com/QuentinWatt
* https://www.instagram.com/QuentinWatt